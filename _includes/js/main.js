// Global Variables
var map;

// Tooltips & Popovers
$(function () {
  $('[data-toggle="popover"]').popover({
  trigger: 'focus'
})
});

// Smooth Scroll
$('a[href*="#"]')
// Remove links that don't actually link to anything
  .not('[href="#"]')
  .not('[href="#0"]')
  .click(function(event) {
  // On-page links
  if (
    location.pathname.replace(/^\//, "") ==
    this.pathname.replace(/^\//, "") &&
    location.hostname == this.hostname
  ) {
    // Figure out element to scroll to
    var target = $(this.hash);
    target = target.length ? target : $("[name=" + this.hash.slice(1) + "]");
    // Does a scroll target exist?
    if (target.length) {
      // Only prevent default if animation is actually gonna happen
      event.preventDefault();
      $("html, body").stop().animate(
        {
          scrollTop: target.offset().top - $(".navbar").height()
        },
        1000,
        function() {
          // Callback after animation
          // Must change focus!
          var $target = $(target);
          $target.focus();
          if ($target.is(":focus")) {
            // Checking if the target was focused
            return false;
          } else {
            $target.attr("tabindex", "-1"); // Adding tabindex for elements not focusable
            $target.focus(); // Set focus again
          }
        }
      );
    }
  }
});

// Active Navigation on Scroll
$(document).ready(function() {
  $(document).on("scroll", function onScroll(event) {
    var scrollPos = $(document).scrollTop();
    $(".nav-link").each(function() {
      var currLink = $(this).parent();
      var refElement = $($(this).attr("href"));
      if (
        refElement.position().top - 200 <= scrollPos &&
        (refElement.position().top - 200) + refElement.height() > scrollPos
      ) {
        $(".nav-link").parents().removeClass("active");
        currLink.addClass("active");
      } else {
        currLink.parent().removeClass("active");
      }
    });
  });
});

// Active Navigation on Click
$('.nav-link').click(function(){
  $('.active').removeClass('active');
  $(this).parent().addClass('active');
});

// Sticky Navigation
$(window).scroll(function(){
    if($(window).scrollTop() > (75)){
       $(".navbar").css("background","#0f2947");
       $(".navbar .navbar-brand img").css("background","#1d5292");
       $(".navbar .navbar-brand img").css("box-shadow","0 0 50px #0f2947");
    }
    else{
       $(".navbar").css("background","transparent");
       $(".navbar .navbar-brand img").css("background","transparent");
       $(".navbar .navbar-brand img").css("box-shadow","none");
    }
});

// Google Map
$(function() {
  function myMap() {
    map = new google.maps.Map(document.getElementById("googleMap"), {
      center: new google.maps.LatLng(9.029941, -79.511376),
      zoom: 15,
      scrollwheel: false,
      zoomControl: false,
      navigationControl: false,
      mapTypeControl: false,
      scaleControl: false,
      disableDoubleClickZoom: true,
      streetViewControl: false,
      draggable: false,
      mapTypeId: google.maps.MapTypeId.ROADMAP
    });
    
    var customMarker = {
      url: 'https://s3-us-west-2.amazonaws.com/s.cdpn.io/347185/waypoint-icon.svg',
    size: new google.maps.Size(40, 64),
    origin: new google.maps.Point(0, 0),
    anchor: new google.maps.Point(0, 64)
    }
    
    var contentString = '<div id="content">'+
            '<div id="siteNotice">'+
            '</div>'+
            '<h1 id="firstHeading" class="firstHeading">Uluru</h1>'+
            '<div id="bodyContent">'+
            '<p><b>Uluru</b>, also referred to as <b>Ayers Rock</b>, is a large ' +
            'sandstone rock formation in the southern part of the '+
            'Northern Territory, central Australia. It lies 335&#160;km (208&#160;mi) '+
            'south west of the nearest large town, Alice Springs; 450&#160;km '+
            '(280&#160;mi) by road. Kata Tjuta and Uluru are the two major '+
            'features of the Uluru - Kata Tjuta National Park. Uluru is '+
            'sacred to the Pitjantjatjara and Yankunytjatjara, the '+
            'Aboriginal people of the area. It has many springs, waterholes, '+
            'rock caves and ancient paintings. Uluru is listed as a World '+
            'Heritage Site.</p>'+
            '<p>Attribution: Uluru, <a href="https://en.wikipedia.org/w/index.php?title=Uluru&oldid=297882194">'+
            'https://en.wikipedia.org/w/index.php?title=Uluru</a> '+
            '(last visited June 22, 2009).</p>'+
            '</div>'+
            '</div>';

        var infowindow = new google.maps.InfoWindow({
          content: contentString
        });
    
    cfPanama = new google.maps.Marker({
      map: map,
      position: { lat: 9.029941, lng: -79.511376 },
      icon: customMarker,
      title: 'Hello'
    });
    
    cfPanama.addListener('click', function() {
        console.log("boop!");
          infowindow.open(map, cfPanama);
        });
  }
  myMap();
});

// Add slideDown animation to Bootstrap dropdown when expanding.
$(".dropdown").on("show.bs.dropdown", function() {
  $(this).find(".dropdown-menu").first().stop(true, true).slideDown();
});

// Add slideUp animation to Bootstrap dropdown when collapsing.
$(".dropdown").on("hide.bs.dropdown", function() {
  $(this).find(".dropdown-menu").first().stop(true, true).slideUp();
});

// Dropdown Button
$(".dropdown-menu a").click(function() {
  var selText = $(this).text();
  
  if ($(this).hasClass("cf")) {
    switch (selText) {
    case "Chorrera":
        map.panTo(new google.maps.LatLng(9, -80));
        break;
    case "Gorgona":
        map.panTo(new google.maps.LatLng(10, -81));
        break;
    case "San Carlos":
        map.panTo(new google.maps.LatLng(11, -82));
        break;
    case "Rio Hato":
        map.panTo(new google.maps.LatLng(12, -83));
        break;
    case "Anton":
        map.panTo(new google.maps.LatLng(13, -84));
        break;
    case "Colon":
        map.panTo(new google.maps.LatLng(14, -85));
        break;
    case "Panama":
        map.panTo(new google.maps.LatLng(15, -86));
      }
    
    $(this).parents(".dropdown").find(".btn-dropdown-text").html('Cuarto Frio - ' + selText);
  } else {
    switch (selText) {
    case "Panama":
        map.panTo(new google.maps.LatLng(9.029941, -79.511376));
        break;
    case "Chorrera":
        map.panTo(new google.maps.LatLng(17, -88));
      }
    $(this).parents(".dropdown").find(".btn-dropdown-text").html('Fabrica de ' + selText);
  }
});

// Burger Animation
$(function(){
	var $burger = $('.burger');
	var $bars = $('.burger-svg__bars');
	var $bar = $('.burger-svg__bar');
	var $bar1 = $('.burger-svg__bar-1');
	var $bar2 = $('.burger-svg__bar-2');
	var $bar3 = $('.burger-svg__bar-3');
	var isChangingState = false;
	var isOpen = false;
	var burgerTL = new TimelineMax();
	
	function burgerOver() {
			
		if(!isChangingState) {
			burgerTL.clear();
			if(!isOpen) {
				burgerTL.to($bar1, 0.5, { y: -2, ease: Elastic.easeOut })
						.to($bar2, 0.5, { scaleX: 0.6, ease: Elastic.easeOut, transformOrigin: "50% 50%" }, "-=0.5")
						.to($bar3, 0.5, { y: 2, ease: Elastic.easeOut }, "-=0.5");
			}
			else {
				burgerTL.to($bar1, 0.5, { scaleX: 1.2, ease: Elastic.easeOut })
						.to($bar3, 0.5, { scaleX: 1.2, ease: Elastic.easeOut }, "-=0.5");
			}
		}
	}
	
	function burgerOut() {
		if(!isChangingState) {
			burgerTL.clear();
			if(!isOpen) {
				burgerTL.to($bar1, 0.5, { y: 0, ease: Elastic.easeOut })
						.to($bar2, 0.5, { scaleX: 1, ease: Elastic.easeOut, transformOrigin: "50% 50%" }, "-=0.5")
						.to($bar3, 0.5, { y: 0, ease: Elastic.easeOut }, "-=0.5");
			}
			else {
				burgerTL.to($bar1, 0.5, { scaleX: 1, ease: Elastic.easeOut })
						.to($bar3, 0.5, { scaleX: 1, ease: Elastic.easeOut }, "-=0.5");
			}
		}
	}
	
	function showCloseBurger() {
		burgerTL.clear();
		burgerTL.to($bar1, 0.3, { y: 6, ease: Power4.easeIn })
				.to($bar2, 0.3, { scaleX: 1, ease: Power4.easeIn }, "-=0.3")
				.to($bar3, 0.3, { y: -6, ease: Power4.easeIn }, "-=0.3")
				.to($bar1, 0.5, { rotation: 45, ease: Elastic.easeOut, transformOrigin: "50% 50%" })
				.set($bar2, { opacity: 0, immediateRender: false }, "-=0.5")
				.to($bar3, 0.5, { rotation: -45, ease: Elastic.easeOut, transformOrigin: "50% 50%", onComplete: function() { isChangingState = false; isOpen = true; } }, "-=0.5");
	}
	
	function showOpenBurger() {
		burgerTL.clear();
		burgerTL.to($bar1, 0.3, { scaleX: 0, ease: Back.easeIn })
				.to($bar3, 0.3, { scaleX: 0, ease: Back.easeIn }, "-=0.3")
				.set($bar1, { rotation: 0, y: 0 })
				.set($bar2, { scaleX: 0, opacity: 1 })
				.set($bar3, { rotation: 0, y: 0 })
				.to($bar2, 0.5, { scaleX: 1, ease: Elastic.easeOut })
				.to($bar1, 0.5, { scaleX: 1, ease: Elastic.easeOut }, "-=0.4")
				.to($bar3, 0.5, { scaleX: 1, ease: Elastic.easeOut, onComplete: function() { isChangingState = false; isOpen = false; } }, "-=0.5");
	}

	$burger.on('click', function(e) {
		
		if(!isChangingState) {
			isChangingState = true;
		
			if(!isOpen) {
				showCloseBurger();
			}
			else {
				showOpenBurger();
			}
		}
		
	});
	
	$burger.hover( burgerOver, burgerOut );
	
});

// Mobile Navigation
$('.burger, .mobile-nav-item').click(function(){
  $('.inner-wrapper').toggleClass('toggled-body');
})

// Mobile Navigation
$('.burger, .mobile-nav-item').click(function(){
  $('.mobile-menu').toggleClass('toggled-menu');
})

// Numbers Count
var $numbers = $('.counter-value');
var $window = $(window);

function check_if_in_view() {
  var window_height = $window.height();
  var window_top_position = $window.scrollTop();
  var window_bottom_position = window_top_position + window_height;

  $.each($numbers, function() {
    var $numbers = $(this);
    var numbers_height = $numbers.outerHeight();
    var numbers_top_position = $numbers.offset().top;
    var numbers_bottom_position = numbers_top_position + numbers_height;

    //check to see if this current container is within viewport
    if (
      numbers_bottom_position >= window_top_position &&
      numbers_top_position <= window_bottom_position
    ) {
      $numbers.prop("Counter", 0).animate(
        {
          Counter: $(this).attr("data-count")
        },
        {
          duration: 1000,
          easing: "swing",
          step: function(now) {
            $(this).text(Math.ceil(now));
          }
        }
      );
    }
  });
}

$window.on('scroll resize', check_if_in_view);

var $window           = $(window),
    win_height_padded = $window.height() * 1.1,
    isTouch           = Modernizr.touch;

function revealOnScroll() {
    var scrolled = $window.scrollTop();
    $(".revealOnScroll:not(.animated)").each(function () {
      var $this     = $(this),
          offsetTop = $this.offset().top + 200;

      if (scrolled + win_height_padded > offsetTop) {
        if ($this.data('timeout')) {
          window.setTimeout(function(){
            $this.addClass('animated ' + $this.data('animation'));
          }, parseInt($this.data('timeout'),10));
        } else {
          $this.addClass('animated ' + $this.data('animation'));
        }
      }
    })
};

$window.on('scroll', revealOnScroll);
$(window).trigger('scroll');